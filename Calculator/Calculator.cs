﻿using System;

namespace CalculatorLibrary
{
    public class Calculator
    {
        public double calculateTriangleArea(double baseLength, double height)
        {
            double result = 0;
            result = (baseLength * height) / 2;
            return result;
        }

        public double calculateTrianglePerimeter(double sidea, double sideb, double sidec)
        {
            double result = 0;
            result = sidea + sideb + sidec;
            return result;
        }

        public double calculateRectangleArea(double sidea, double sideb)
        {
            double result = 0;
            result = sidea * sideb;
            return result;
        }

        public double calculateRectanglePerimeter(double sidea, double sideb)
        {
            double result = 0;
            result = (2 * sidea) + (2 * sideb);
            return result;
        }

        public double calculateCircleArea(double radius)
        {
            double result = 0;
            result = (Math.PI) * (radius * radius);
            return result;
        }

        public double calculateCirclePerimeter(double radius)
        {
            double result = 0;
            result = 2 * Math.PI * radius;
            return result;
        }

        public double calculatePrismVolume(double sidea, double sideb, double sidec)
        {
            double result = 0;
            result = sidea *sideb * sidec;
            return result;
        }

        public double calculatePrismSurfaceArea(double sidea, double sideb, double sidec)
        {
            double result = 0;
            result = (2 * sidea * sideb) + (2 * sideb * sidec) + (2 * sidea * sidec);
            return result;
        }

        public double calculateSphereVolume(double radius)
        {
            double result = 0;
            result = (4 * Math.PI * (radius * radius * radius)) / 3;
            return result;
        }

        public double calculateSphereSurfaceArea(double radius)
        {
            double result = 0;
            result = 4 * Math.PI * (radius * radius);
            return result;
        }
    }

}
