﻿using System;
using CalculatorLibrary;

namespace Shapes_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            bool end = false;

            while(!end)
            {
                int dimension = 0;
                char shape = 'z';
                char operation = 'z';
                Calculator calculator = new Calculator();

                Console.WriteLine("Would you like to perform calculations for a 2D or 3D shape?");
                Console.WriteLine("Enter 2 for 2D or 3 for 3D.");
                dimension = Convert.ToInt32(Console.ReadLine());

                while(dimension != 2 && dimension != 3)
                {
                    Console.WriteLine("That is not a valid input.  Please enter 2 for 2D or 3 for 3D.");
                    dimension = Convert.ToInt32(Console.ReadLine());
                }

                if(dimension == 2)
                {
                    Console.WriteLine("Choose a shape from the following list:");
                    Console.WriteLine("\tt - Triangle");
                    Console.WriteLine("\tr - Rectangle");
                    Console.WriteLine("\tc - Circle");
                    shape = Convert.ToChar(Console.ReadLine());

                    while(shape != 't' && shape !='r' && shape !='c')
                    {
                        Console.WriteLine("That is not a valid input.  Choose a shape from the following list:");
                        Console.WriteLine("\tt - Triangle");
                        Console.WriteLine("\tr - Rectangle");
                        Console.WriteLine("\tc - Circle");
                        shape = Convert.ToChar(Console.ReadLine());
                    }

                    if(shape == 't')
                    {
                        Console.WriteLine("Would you like to calculator area or perimeter?  Enter a for area or p for perimeter.");
                        operation = Convert.ToChar(Console.ReadLine());

                        while (operation != 'a' && operation != 'p')
                        {
                            Console.WriteLine("That is not a valid input.  Enter a for area or p for perimeter.");
                            operation = Convert.ToChar(Console.ReadLine());
                        }

                        if(operation == 'a')
                        {
                            Console.WriteLine("Enter the length of the triangle's base.");
                            double baseLength = Convert.ToDouble(Console.ReadLine());

                            Console.WriteLine("Enter the triangle's height.");
                            double height = Convert.ToDouble(Console.ReadLine());

                            double result = calculator.calculateTriangleArea(baseLength, height);
                            Console.WriteLine("The area of your triangle is " + result);
                        }

                        else
                        {
                            Console.WriteLine("Enter the length of the triangle's first side.");
                            double sidea = Convert.ToDouble(Console.ReadLine());

                            Console.WriteLine("Enter the length of the triangle's second side.");
                            double sideb = Convert.ToDouble(Console.ReadLine());

                            Console.WriteLine("Enter the length of the triangle's third side.");
                            double sidec = Convert.ToDouble(Console.ReadLine());

                            double result = calculator.calculateTrianglePerimeter(sidea, sideb, sidec);
                            Console.WriteLine("The perimeter of your triangle is " + result);
                        }

                    }

                    if (shape == 'r')
                    {
                        Console.WriteLine("Would you like to calculator area or perimeter?  Enter a for area or p for perimeter.");
                        operation = Convert.ToChar(Console.ReadLine());

                        while (operation != 'a' && operation != 'p')
                        {
                            Console.WriteLine("That is not a valid input.  Enter a for area or p for perimeter.");
                            operation = Convert.ToChar(Console.ReadLine());
                        }

                        if (operation == 'a')
                        {
                            Console.WriteLine("Enter the length of one of the rectangle's sides.");
                            double sidea = Convert.ToDouble(Console.ReadLine());

                            Console.WriteLine("Enter the length of a perpendicular side.");
                            double sideb = Convert.ToDouble(Console.ReadLine());

                            double result = calculator.calculateRectangleArea(sidea, sideb);
                            Console.WriteLine("The area of your rectangle is " + result);
                        }

                        else
                        {
                            Console.WriteLine("Enter the length of one of the rectangle's sides.");
                            double sidea = Convert.ToDouble(Console.ReadLine());

                            Console.WriteLine("Enter the length of a perpendicular side.");
                            double sideb = Convert.ToDouble(Console.ReadLine());

                            double result = calculator.calculateRectanglePerimeter(sidea, sideb);
                            Console.WriteLine("The perimeter of your rectangle is " + result);
                        }

                    }

                    else
                    {
                        Console.WriteLine("Would you like to calculator area or perimeter?  Enter a for area or p for perimeter.");
                        operation = Convert.ToChar(Console.ReadLine());

                        while (operation != 'a' && operation != 'p')
                        {
                            Console.WriteLine("That is not a valid input.  Enter a for area or p for perimeter.");
                            operation = Convert.ToChar(Console.ReadLine());
                        }

                        if (operation == 'a')
                        {
                            Console.WriteLine("Enter the length of the circle's radius.");
                            double radius = Convert.ToDouble(Console.ReadLine());

                            double result = calculator.calculateCircleArea(radius);
                            Console.WriteLine("The area of your circle is " + result);
                        }

                        else
                        {
                            Console.WriteLine("Enter the length of othe circle's radius.");
                            double radius = Convert.ToDouble(Console.ReadLine());

                            double result = calculator.calculateCirclePerimeter(radius);
                            Console.WriteLine("The perimeter of your circle is " + result);
                        }

                    }

                }

                else
                {
                    Console.WriteLine("Choose a shape from the following list:");
                    Console.WriteLine("\tr - Rectangular Prism");
                    Console.WriteLine("\ts - Sphere");
                    shape = Convert.ToChar(Console.ReadLine());

                    while (shape != 'r' && shape != 's')
                    {
                        Console.WriteLine("That is not a valid input.  Choose a shape from the following list:");
                        Console.WriteLine("\tr - Rectangular Prism");
                        Console.WriteLine("\tc - Sphere");
                        shape = Convert.ToChar(Console.ReadLine());
                    }

                    if (shape == 'r')
                    {
                        Console.WriteLine("Would you like to calculator surface area or volume ?  Enter a for surface area or v for volume.");
                        operation = Convert.ToChar(Console.ReadLine());

                        while (operation != 'a' && operation != 'v')
                        {
                            Console.WriteLine("That is not a valid input.  Enter a for surface area or v for volume.");
                            operation = Convert.ToChar(Console.ReadLine());
                        }

                        if (operation == 'a')
                        {
                            Console.WriteLine("Enter the length of the prism's base.");
                            double sidea = Convert.ToDouble(Console.ReadLine());

                            Console.WriteLine("Enter the length of the prism's height.");
                            double sideb = Convert.ToDouble(Console.ReadLine());

                            Console.WriteLine("Enter the length of the prism's width.");
                            double sidec = Convert.ToDouble(Console.ReadLine());

                            double result = calculator.calculatePrismSurfaceArea(sidea, sideb, sidec);
                            Console.WriteLine("The surface area of your rectangular prism is " + result);
                        }

                        else
                        {
                            Console.WriteLine("Enter the length of the prism's base.");
                            double sidea = Convert.ToDouble(Console.ReadLine());

                            Console.WriteLine("Enter the length of the prism's height.");
                            double sideb = Convert.ToDouble(Console.ReadLine());

                            Console.WriteLine("Enter the length of the prism's width.");
                            double sidec = Convert.ToDouble(Console.ReadLine());

                            double result = calculator.calculatePrismVolume(sidea, sideb, sidec);
                            Console.WriteLine("The volume of your rectangular prism is " + result);
                        }

                    }

                    else
                    {
                        Console.WriteLine("Would you like to calculator surface area or volume ?  Enter a for surface area or v for volume.");
                        operation = Convert.ToChar(Console.ReadLine());

                        while (operation != 'a' && operation != 'v')
                        {
                            Console.WriteLine("That is not a valid input.  Enter a for surface area or v for volume.");
                            operation = Convert.ToChar(Console.ReadLine());
                        }

                        if (operation == 'a')
                        {
                            Console.WriteLine("Enter the length of the sphere's radius.");
                            double radius = Convert.ToDouble(Console.ReadLine());

                            double result = calculator.calculateSphereSurfaceArea(radius);
                            Console.WriteLine("The surface area of your sphere is " + result);
                        }

                        else
                        {
                            Console.WriteLine("Enter the length of the sphere's radius.");
                            double radius = Convert.ToDouble(Console.ReadLine());

                            double result = calculator.calculateSphereVolume(radius);
                            Console.WriteLine("The volume of your sphere is " + result); ;
                        }

                    }
                }
            }
        }
    }
}
