# Calculator_Console

Selecting one of four operations (perimeter, area, surface area, volume) will display two or three shapes and the appropriate number of parameters to perform the calculation.  Once the calculation is performed, the shape, operation, paramters, and result are saved to a database so they can be accessed later through the search history button.  Users can select the options button to select the number of decimal places in the result and the number of entries in the search history.  The decimal places option has a range of 1 - 10, and the history option has a range of 5 - 20.

The password required to use the access the database has been removed, so the application will not function in this state.
